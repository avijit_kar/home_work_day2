
<?php
$input = "avijit";
echo str_pad($input, 10);
echo "<br> ";
echo str_pad($input, 10, "-=", STR_PAD_LEFT);
echo "<br> ";// produces "-=-=-Alien"
echo str_pad($input, 10, "_", STR_PAD_BOTH);
echo "<br> ";// produces "__Alien___"
echo str_pad($input,  10, "___");
echo "<br> ";// produces "Alien_"
echo str_pad($input,  10, "*");                 // produces "Alien"
?>